package com.gbarzy.godwin.teecha;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Created by TEXT-GODWIN on 10/9/2018.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "hello.db";
    public static final int DATABASE_VERSION = 1;
    private final static String COL_KEY = "key";
    private final static String COL_VALUE = "value";
    private static final String COL_CAT = "category";
    public static SQLiteDatabase mDB;
    private final String TBL_AGRI = "agriculture";
    private final String TBL_CHE = "chemistry";
    private final String TBL_ECO = "economics";
    private final String TBL_ENG = "mechanical_engineering";
    private final String TBL_ART = "art";
    private final String TBL_BIO = "biology";
    private final String TBL_ACC = "accounting";
    private final String TBL_DES = "graphic_design";
    private final String TBL_PHY = "physics";
    private final String TBL_GEO = "geography";
    private final String TBL_GOV = "government";
    private final String TBL_MAT = "mathematics";
    private final String TBL_MAN = "management";
    private final String TBL_MUS = "music";
    private final String TBL_COS = "costing";
    private final String TBL_FOD = "food_nutrition";
    private final String TBL_COM = "computing";
    private final String TBL_APE = "applied_electricity";

    private final String TBL_BOOKMARK = "bookmark";
    private Context mContext;
    private String DATABASE_LOCATION = "";
    private String DATABASE_FULL_PATH = "";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mContext = context;
        DATABASE_LOCATION = "data/data/" + mContext.getPackageName() + "/database/";
        DATABASE_FULL_PATH = DATABASE_LOCATION + DATABASE_NAME;


        if (!existingDB()) {

            try {

                //create directory before copying database
                File dbLocation = new File((DATABASE_LOCATION));
                dbLocation.mkdir();

                extraAssetToDatatbaseDirectory(DATABASE_NAME);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        mDB = SQLiteDatabase.openOrCreateDatabase(DATABASE_FULL_PATH, null);
    }

    public static void removeBookmark(String key) {
        try {

            String q = "DELETE FROM bookmark WHERE([" + COL_KEY + "]) = ?";
            mDB.execSQL(q, new Object[]{key});
        } catch (SQLiteException ex) {

        }
    }

    public static void clearBookmark() {
        try {

            String q = "DELETE FROM bookmark;";
            mDB.execSQL(q);
        } catch (SQLiteException ex) {
            Log.e("error clearing bookmark", ex.getMessage());
            ex.printStackTrace();
        }
    }

    boolean existingDB() {
        File file = new File(DATABASE_FULL_PATH);
        return file.exists();
    }

    public void extraAssetToDatatbaseDirectory(String filename)
            throws IOException//copy the database

    {
        int length;
        InputStream sourceDatabase = this.mContext.getAssets().open(filename);
        File destinationPath = new File(DATABASE_FULL_PATH);
        OutputStream destination = new FileOutputStream(destinationPath);

        byte[] buffer = new byte[4096];
        while ((length = sourceDatabase.read(buffer)) > 0) {
            destination.write(buffer, 0, length);
        }
        sourceDatabase.close();
        destination.flush();
        destination.close();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public ArrayList<String> getWord(int dicType) {

        String tableName = getTableName(dicType);

        String q = "SELECT * FROM " + tableName;
        Cursor result = mDB.rawQuery(q, null);

        ArrayList<String> source = new ArrayList<>();
        while (result.moveToNext()) {
            source.add(result.getString(result.getColumnIndex(COL_KEY)));
        }
        return source;
    }

    public Word getWord(String key, int dicType) {

        String tableName = getTableName(dicType);

        String q = "SELECT * FROM " + tableName + " WHERE upper([key]) = upper(?)";
        Cursor result = mDB.rawQuery(q, new String[]{key});

        Word word = new Word();
        while (result.moveToNext()) {
            word.key = result.getString(result.getColumnIndex(COL_KEY));
            word.value = result.getString(result.getColumnIndex(COL_VALUE));
        }
        return word;
    }

    public void addBookmark(Word word, int category) {
        try {
            String q = "INSERT INTO bookmark([" + COL_KEY + "],[" + COL_VALUE + "],[" + COL_CAT + "]) VALUES(?, ?, ?);";
            mDB.execSQL(q, new Object[]{word.key, word.value, category});
        } catch (SQLiteException ex) {

        }
    }

    public void removeBookmark(Word word) {
        try {

            String q = "DELETE FROM bookmark WHERE([" + COL_KEY + "]) = upper(?)  AND ([" + COL_VALUE + "]) = ?;";
            mDB.execSQL(q, new String[]{word.key, word.value});
        } catch (SQLiteException ex) {
            Log.e("delete error", ex.getMessage());
            ex.printStackTrace();
        }
    }

    public ArrayList<String> getAllWordFromBookmark(String key) {

        String q = "SELECT * FROM bookmark ORDER BY [date] DESC;";
        Cursor result = mDB.rawQuery(q, new String[]{});

        ArrayList<String> source = new ArrayList<>();
        while (result.moveToNext()) {
            source.add(result.getString(result.getColumnIndex(COL_KEY)));
        }
        return source;
    }

    public ArrayList<Word> getAllWordFromBookmark() {
        String q = "SELECT * FROM bookmark ORDER BY [category], [key];";
        Cursor result = mDB.rawQuery(q, new String[]{});

        ArrayList<Word> source = new ArrayList<>();
        while (result.moveToNext()) {
            source.add(
                    new Word(result.getString(result.getColumnIndex(COL_KEY)),
                            result.getString(result.getColumnIndex(COL_VALUE)),
                            getTableName(result.getInt(result.getColumnIndex(COL_CAT)))
                    ));
        }
        return source;
    }

    public boolean isWordMarked(Word word) {
        String q = "SELECT * FROM bookmark WHERE upper([key]) = upper(?) AND [value] = ?";
        Cursor result = mDB.rawQuery(q, new String[]{word.key, word.value});
        return result.getCount() > 0;

    }

    public Word getWordFromBookmark(String key) {
        String q = "SELECT * FROM bookmark WHERE upper([key]) = upper(?) AND [value] = ''";
        Cursor result = mDB.rawQuery(q, null);
        Word word = new Word();
        while (result.moveToNext()) {
            word.key = result.getString(result.getColumnIndex(COL_KEY));
            word.value = result.getString(result.getColumnIndex(COL_VALUE));
        }

        return word;
    }

    public String getTableName(int dicType) {

        String tableName = "";
        if (dicType == R.id.action_agriculture) {
            tableName = TBL_AGRI;
        } else if (dicType == R.id.action_economics) {
            tableName = TBL_ECO;
        } else if (dicType == R.id.action_graphic_design) {
            tableName = TBL_DES;
        } else if (dicType == R.id.action_management) {
            tableName = TBL_MAN;
        } else if (dicType == R.id.action_mathematics) {
            tableName = TBL_MAT;
        } else if (dicType == R.id.action_biology) {
            tableName = TBL_BIO;
        } else if (dicType == R.id.action_physics) {
            tableName = TBL_PHY;
        } else if (dicType == R.id.action_accounting) {
            tableName = TBL_ACC;
        } else if (dicType == R.id.action_art) {
            tableName = TBL_ART;
        } else if (dicType == R.id.action_chemistry) {
            tableName = TBL_CHE;
        } else if (dicType == R.id.action_geography) {
            tableName = TBL_GEO;
        } else if (dicType == R.id.action_music) {
            tableName = TBL_MUS;
        } else if (dicType == R.id.action_engineering) {
            tableName = TBL_ENG;
        } else if (dicType == R.id.action_government) {
            tableName = TBL_GOV;
        } else if (dicType == R.id.action_costing) {
            tableName = TBL_COS;
        } else if (dicType == R.id.action_food_nutrition) {
            tableName = TBL_FOD;
        } else if (dicType == R.id.action_computing) {
            tableName = TBL_COM;
        } else if (dicType == R.id.action_applied_electricity) {
            tableName = TBL_APE;
        }else if (dicType == 0){
            tableName = TBL_BOOKMARK;
        }


        return tableName;
    }
}
