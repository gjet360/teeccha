package com.gbarzy.godwin.teecha;

/**
 * Created by TEXT-GODWIN on 10/9/2018.
 */
public interface ListItemListener {
    void onItemClick(int position);
}
