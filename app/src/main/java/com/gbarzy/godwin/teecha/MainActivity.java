package com.gbarzy.godwin.teecha;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    MenuItem menuSetting;
    Toolbar toolbar;

    DBHelper dbHelper;

    DictionaryFragment dictionaryFragment;
    BookmarkFragment bookmarkFragment;
    AutoCompleteTextView edit_search;
    //DetailFragment detailFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        dbHelper = new DBHelper(this);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        dictionaryFragment = new DictionaryFragment();
        bookmarkFragment = new BookmarkFragment();
        gotoFragment(dictionaryFragment, true);
        //detailFragment = new DetailFragment();

        dictionaryFragment.setOnFragmentListener(new FragmentListener() {
            @Override
            public void onItemClick(String value) {
//                Global.saveState(MainActivity.this, "dic_type");
//                Toast.makeText(MainActivity.this,value,Toast.LENGTH_SHORT).show();
                gotoFragment(DetailFragment.getNewInstance(value), false);
            }
        });

        bookmarkFragment.setOnFragmentListener(new FragmentListener() {
            @Override
            public void onItemClick(String value) {
                // Toast.makeText(MainActivity.this,value,Toast.LENGTH_SHORT).show();
                gotoFragment(DetailFragment.getNewInstance(value, true), false);
            }
        });

        edit_search = findViewById(R.id.edit_search);
        edit_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                dictionaryFragment.filterValue(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }


    @Override
    public void onBackPressed() {
        this.edit_search.setText("");
        this.edit_search.setThreshold(99999);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        menuSetting = menu.findItem(R.id.action_settings);

        String id = Global.getState(this, "dic_type");

        Log.i("sas", "aef");

        if (id.length() > 0) {

            //onCreateOptionsMenu((Menu) menu.findItem(Integer.parseInt(((id)))));

//            menu.findItem(Integer.parseInt(id));
            ArrayList<String> source = dbHelper.getWord(Integer.parseInt(id));
            dictionaryFragment.resetDataSource(source);

//            menu.getItem()

//            Toast.makeText(MainActivity.this, "id = "+id, Toast.LENGTH_SHORT).show();
        } else {
            Global.saveState(this, "dic_type", String.valueOf(R.id.action_agriculture));
            ArrayList<String> source = dbHelper.getWord(R.id.action_agriculture);
            dictionaryFragment.resetDataSource(source);
        }


        super.onCreateOptionsMenu(menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
/*        if (id == R.id.action_agriculture) {
            Global.saveState(this, "dic_type", String.valueOf(id));
            ArrayList<String> source = dbHelper.getWord(id);
            dictionaryFragment.resetDataSource(source);
            menuSetting.setIcon(getDrawable(R.drawable.ic_agriculture));
            return true;
        } else if (id == R.id.action_chemistry) {

            Global.saveState(this, "dic_type", String.valueOf(id));
            ArrayList<String> source = dbHelper.getWord(id);
            dictionaryFragment.resetDataSource(source);
            menuSetting.setIcon(getDrawable(R.drawable.ic_chemistry));
            return true;
        } else if (id == R.id.action_accounting) {
            Global.saveState(this, "dic_type", String.valueOf(id));
            ArrayList<String> source = dbHelper.getWord(id);
            dictionaryFragment.resetDataSource(source);
            menuSetting.setIcon(getDrawable(R.drawable.ic_accounting));
            return true;
        } else if (id == R.id.action_graphic_design) {
            Global.saveState(this, "dic_type", String.valueOf(id));
            ArrayList<String> source = dbHelper.getWord(id);
            dictionaryFragment.resetDataSource(source);
            menuSetting.setIcon(getDrawable(R.drawable.ic_graphic_design));
            return true;
        } else if (id == R.id.action_economics) {
            Global.saveState(this, "dic_type", String.valueOf(id));
            ArrayList<String> source = dbHelper.getWord(id);
            dictionaryFragment.resetDataSource(source);
            menuSetting.setIcon(getDrawable(R.drawable.ic_econimics));
            return true;
        } else if (id == R.id.action_engineering) {
            Global.saveState(this, "dic_type", String.valueOf(id));
            ArrayList<String> source = dbHelper.getWord(id);
            dictionaryFragment.resetDataSource(source);
            menuSetting.setIcon(getDrawable(R.drawable.ic_mechanical_engineering));
            return true;
        } else if (id == R.id.action_art) {
            Global.saveState(this, "dic_type", String.valueOf(id));
            ArrayList<String> source = dbHelper.getWord(id);
            dictionaryFragment.resetDataSource(source);
            menuSetting.setIcon(getDrawable(R.drawable.ic_art));
            return true;
        } else if (id == R.id.action_physics) {
            Global.saveState(this, "dic_type", String.valueOf(id));
            ArrayList<String> source = dbHelper.getWord(id);
            dictionaryFragment.resetDataSource(source);
            menuSetting.setIcon(getDrawable(R.drawable.ic_physics));
            return true;
        } else if (id == R.id.action_geography) {
            Global.saveState(this, "dic_type", String.valueOf(id));
            ArrayList<String> source = dbHelper.getWord(id);
            dictionaryFragment.resetDataSource(source);
            menuSetting.setIcon(getDrawable(R.drawable.ic_geography));
            return true;
        } else if (id == R.id.action_government) {
            Global.saveState(this, "dic_type", String.valueOf(id));
            ArrayList<String> source = dbHelper.getWord(id);
            dictionaryFragment.resetDataSource(source);
            menuSetting.setIcon(getDrawable(R.drawable.ic_governemt));
            return true;
        } else if (id == R.id.action_biology) {
            Global.saveState(this, "dic_type", String.valueOf(id));
            ArrayList<String> source = dbHelper.getWord(id);
            dictionaryFragment.resetDataSource(source);
            menuSetting.setIcon(getDrawable(R.drawable.ic_biology));
            return true;
        } else if (id == R.id.action_music) {
            Global.saveState(this, "dic_type", String.valueOf(id));
            ArrayList<String> source = dbHelper.getWord(id);
            dictionaryFragment.resetDataSource(source);
            menuSetting.setIcon(getDrawable(R.drawable.ic_music));
            return true;
        } else if (id == R.id.action_mathematics) {
            Global.saveState(this, "dic_type", String.valueOf(id));
            ArrayList<String> source = dbHelper.getWord(id);
            dictionaryFragment.resetDataSource(source);
            menuSetting.setIcon(getDrawable(R.drawable.ic_mathematics));
            return true;
        } else if (id == R.id.action_management) {
            Global.saveState(this, "dic_type", String.valueOf(id));
            ArrayList<String> source = dbHelper.getWord(id);
            dictionaryFragment.resetDataSource(source);
            menuSetting.setIcon(getDrawable(R.drawable.ic_management_in_living));
            return true;

        }*/



        int selected_id = 0;
        int icon = 0;

        switch (item.getItemId()) {
            case R.id.action_agriculture:
                selected_id = item.getItemId();
                icon = R.drawable.ic_agriculture;
                break;
            case R.id.action_chemistry:
                selected_id = item.getItemId();
                icon = R.drawable.ic_chemistry;

                break;
            case R.id.action_accounting:
                selected_id = item.getItemId();
                icon = R.drawable.ic_accounting;

                break;
            case R.id.action_graphic_design:
                selected_id = item.getItemId();
                icon = R.drawable.ic_graphic_design;

                break;
            case R.id.action_economics:
                selected_id = item.getItemId();
                icon = R.drawable.ic_econimics;

                break;
            case R.id.action_engineering:
                selected_id = item.getItemId();
                icon = R.drawable.ic_mechanical_engineering;

                break;
            case R.id.action_art:
                selected_id = item.getItemId();
                icon = R.drawable.ic_art;

                break;
            case R.id.action_physics:
                selected_id = item.getItemId();
                icon = R.drawable.ic_physics;

                break;
            case R.id.action_geography:
                selected_id = item.getItemId();
                icon = R.drawable.ic_geography;

                break;
            case R.id.action_government:
                selected_id = item.getItemId();
                icon = R.drawable.ic_governemt;

                break;

            case R.id.action_biology:
                selected_id = item.getItemId();
                icon = R.drawable.ic_biology;

                break;

            case R.id.action_music:
                selected_id = item.getItemId();
                icon = R.drawable.ic_music;

                break;

            case R.id.action_mathematics:
                selected_id = item.getItemId();
                icon = R.drawable.ic_mathematics;

                break;

            case R.id.action_management:
                selected_id = item.getItemId();
                icon = R.drawable.ic_management_in_living;

                break;

            case R.id.action_costing:
                selected_id = item.getItemId();
                icon = R.drawable.ic_costing;

                break;

            case R.id.action_food_nutrition:
                selected_id = item.getItemId();
                icon = R.drawable.ic_food_nutrition;

                break;

            case R.id.action_computing:
                selected_id = item.getItemId();
                icon = R.drawable.ic_computing;

                break;

            case R.id.action_applied_electricity:
                selected_id = item.getItemId();
                icon = R.drawable.ic_applied_electricity;

                break;

        }

        if (selected_id > 0){
            Global.saveState(this, "dic_type", String.valueOf(selected_id));
            menuSetting.setIcon(getDrawable(icon));
            ArrayList<String> source = dbHelper.getWord(selected_id);
            dictionaryFragment.resetDataSource(source);

            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_bookmark) {
            String activeFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_Container).getClass().getSimpleName();
            if (!activeFragment.equals(bookmarkFragment.getClass().getSimpleName())) {
                gotoFragment(bookmarkFragment, false);
            }

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void gotoFragment(Fragment fragment, boolean isTop) {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_Container, fragment);
        if (!isTop) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {


        String activeFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_Container).getClass().getSimpleName();
        if (activeFragment.equals(BookmarkFragment.class.getSimpleName())) {
            menuSetting.setVisible(false);
            toolbar.findViewById(R.id.edit_search).setVisibility(View.GONE);
            toolbar.setTitle("Bookmark");
        } else {

            menuSetting.setVisible(true);
            toolbar.findViewById(R.id.edit_search).setVisibility(View.VISIBLE);
            toolbar.setTitle("");
        }
        return super.onPrepareOptionsMenu(menu);


    }
}
