package com.gbarzy.godwin.teecha;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by TEXT-GODWIN on 10/9/2018.
 */
public class BookmarkAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<Word> mSource;
    private ListItemListener listener;
    private ListItemListener listenerBtnDelete;

    public BookmarkAdapter(Context context, ArrayList<Word> source) {
        this.mContext = context;
        this.mSource = source;
    }

    @Override
    public int getCount() {
        return mSource.size();
    }

    @Override
    public Object getItem(int i) {
        return mSource.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public void removeItem(int position) {
        mSource.get(position);
    }

    @Override
    public View getView(final int i, View view, final ViewGroup viewGroup) {
        ViewHolder viewHolder;

        if (view == null) {

            viewHolder = new ViewHolder();
            view = LayoutInflater.from(mContext).inflate(R.layout.bookmark_layout_item, viewGroup, false);
            viewHolder.textView = view.findViewById(R.id.tvWord);
            viewHolder.btnDelete = view.findViewById(R.id.btnDelete);
            viewHolder.tvCategory = view.findViewById(R.id.tvCategory);
            view.setTag(viewHolder);
        } else {

            viewHolder = (ViewHolder) view.getTag();

        }


        viewHolder.textView.setText(mSource.get(i).key);
        viewHolder.tvCategory.setText(mSource.get(i).category.toUpperCase());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onItemClick(i);

            }
        });

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (listenerBtnDelete != null) {
                    listenerBtnDelete.onItemClick(i);
                }
            }
        });

        return view;
    }

    public void delete() {
        mSource.remove(0);
    }

    public void clear() {

        mSource.clear();

    }

    public void setOnItemClick(ListItemListener listItemListener) {
        this.listener = listItemListener;

    }

    public void setOnItemDeleteClick(ListItemListener listItemListener) {
        this.listenerBtnDelete = listItemListener;

    }

    class ViewHolder {
        TextView textView;
        TextView tvCategory;
        ImageView btnDelete;
    }

}
