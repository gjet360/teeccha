package com.gbarzy.godwin.teecha;

/**
 * Created by TEXT-GODWIN on 10/9/2018.
 */
public class DB {
    public static String[]getData(int id){
        if(id==R.id.action_agriculture){
            return getAgriculture();
        }
        else if(id==R.id.action_chemistry){
            return getChemistry();
        }
        else if(id==R.id.action_accounting){
            return getAccounting();
        }
        else if(id==R.id.action_graphic_design){
            return getGraphic();
        }
        else if(id==R.id.action_economics){
            return getEconomics();
        }
        else if(id==R.id.action_engineering){
            return getMechanical();
        }
        else if(id==R.id.action_art){
            return getArt();
        }
        else if(id==R.id.action_physics){
            return getPhysics();
        }
        else if(id==R.id.action_geography){
            return getGeography();
        }
        else if(id==R.id.action_government){
            return getGovernment();
        }
        else if(id==R.id.action_biology){
            return getBiology();
        }
        else if(id==R.id.action_music){
            return getMusic();
        }
        else if(id==R.id.action_mathematics){
            return getMathematics();
        }
        else if(id==R.id.action_management){
            return getManagement();
        }
        return new String[0];
    }


    public static String[]getAgriculture(){
        String[] source = new String[]{

                        "Acid Soil" ,
                        "Acre" ,
                        "Adsorb adsorption",
                        "Agriculture:"   ,
                        "Agriculture Extension Service",
                        "Agronomy",
                        "Alfalfa",
                        "Alluvial Soil",
                        "Animal Unit",
                        "Annual",
                        "Apiary" ,
                        "Aquifer. " ,
                        "Artificial Insemination" ,
                        "Auger" ,
                        "Avian:",
                        "Balance Ration" ,
                        "Banded." ,
                        "Biennial rotation. " ,
                        "Biological control.",
                        "Biomass–energy ",
                        "Biotic. " ,
                        "Bloating: " ,
                        "Boar: " ,
                        "Break-even price." ,
                        "Breed:  "
                                 };
        return source;
    }
    public static String[]getChemistry(){
        String[] source = new String[]{

                "1Acid Soil" ,
                "1Acre" ,
                "1Adsorb adsorption",
                "1Agriculture:"   ,
                "1Agriculture Extension Service",
                "1Agronomy",
                "Alfalfa",
                "Alluvial Soil",
                "Animal Unit",
                "Annual",
                "Apiary" ,
                "Aquifer. " ,
                "Artificial Insemination" ,
                "Auger" ,
                "Avian:",
                "1Balance Ration" ,
                "Banded." ,
                "Biennial rotation. " ,
                "1Biological control.",
                "Biomass–energy ",
                "Biotic. " ,
                "Bloating: " ,
                "Boar: " ,
                "Break-even price." ,
                "Breed:  "
        };
        return source;
    }
    public static String[]getAccounting(){
        String[] source = new String[]{

                "2Acid Soil" ,
                "2Acre" ,
                "2Adsorb adsorption",
                "2Agriculture:"   ,
                "2Agriculture Extension Service",
                "2Agronomy",
                "2Alfalfa",
                "Alluvial Soil",
                "Animal Unit",
                "Annual",
                "Apiary" ,
                "Aquifer. " ,
                "Artificial Insemination" ,
                "Auger" ,
                "Avian:",
                "Balance Ration" ,
                "Banded." ,
                "Biennial rotation. " ,
                "Biological control.",
                "Biomass–energy ",
                "Biotic. " ,
                "Bloating: " ,
                "Boar: " ,
                "Break-even price." ,
                "Breed:  "
        };
        return source;
    }
    public static String[]getGraphic(){
        String[] source = new String[]{

                "Acid Soil" ,
                "Acre" ,
                "Adsorb adsorption",
                "Agriculture:"   ,
                "Agriculture Extension Service",
                "Agronomy",
                "Alfalfa",
                "Alluvial Soil",
                "Animal Unit",
                "Annual",
                "Apiary" ,
                "Aquifer. " ,
                "Artificial Insemination" ,
                "Auger" ,
                "Avian:",
                "Balance Ration" ,
                "Banded." ,
                "Biennial rotation. " ,
                "Biological control.",
                "Biomass–energy ",
                "Biotic. " ,
                "Bloating: " ,
                "Boar: " ,
                "Break-even price." ,
                "Breed:  "
        };
        return source;
    }
    public static String[]getEconomics(){
        String[] source = new String[]{

                "Acid Soil" ,
                "Acre" ,
                "Adsorb adsorption",
                "Agriculture:"   ,
                "Agriculture Extension Service",
                "Agronomy",
                "Alfalfa",
                "Alluvial Soil",
                "Animal Unit",
                "Annual",
                "Apiary" ,
                "Aquifer. " ,
                "Artificial Insemination" ,
                "Auger" ,
                "Avian:",
                "Balance Ration" ,
                "Banded." ,
                "Biennial rotation. " ,
                "Biological control.",
                "Biomass–energy ",
                "Biotic. " ,
                "Bloating: " ,
                "Boar: " ,
                "Break-even price." ,
                "Breed:  "
        };
        return source;
    }
    public static String[]getMechanical(){
        String[] source = new String[]{

                "Acid Soil" ,
                "Acre" ,
                "Adsorb adsorption",
                "Agriculture:"   ,
                "Agriculture Extension Service",
                "Agronomy",
                "Alfalfa",
                "Alluvial Soil",
                "Animal Unit",
                "Annual",
                "Apiary" ,
                "Aquifer. " ,
                "Artificial Insemination" ,
                "Auger" ,
                "Avian:",
                "Balance Ration" ,
                "Banded." ,
                "Biennial rotation. " ,
                "Biological control.",
                "Biomass–energy ",
                "Biotic. " ,
                "Bloating: " ,
                "Boar: " ,
                "Break-even price." ,
                "Breed:  "
        };
        return source;
    }
    public static String[]getArt(){
        String[] source = new String[]{

                "Acid Soil" ,
                "Acre" ,
                "Adsorb adsorption",
                "Agriculture:"   ,
                "Agriculture Extension Service",
                "Agronomy",
                "Alfalfa",
                "Alluvial Soil",
                "Animal Unit",
                "Annual",
                "Apiary" ,
                "Aquifer. " ,
                "Artificial Insemination" ,
                "Auger" ,
                "Avian:",
                "Balance Ration" ,
                "Banded." ,
                "Biennial rotation. " ,
                "Biological control.",
                "Biomass–energy ",
                "Biotic. " ,
                "Bloating: " ,
                "Boar: " ,
                "Break-even price." ,
                "Breed:  "
        };
        return source;
    }
    public static String[]getPhysics(){
        String[] source = new String[]{

                "5Acid Soil" ,
                "5Acre" ,
                "5Adsorb adsorption",
                "5Agriculture:"   ,
                "Agriculture Extension Service",
                "Agronomy",
                "Alfalfa",
                "Alluvial Soil",
                "Animal Unit",
                "Annual",
                "Apiary" ,
                "Aquifer. " ,
                "Artificial Insemination" ,
                "Auger" ,
                "Avian:",
                "Balance Ration" ,
                "Banded." ,
                "Biennial rotation. " ,
                "Biological control.",
                "Biomass–energy ",
                "Biotic. " ,
                "Bloating: " ,
                "Boar: " ,
                "Break-even price." ,
                "Breed:  "
        };
        return source;
    }
    public static String[]getGeography(){
        String[] source = new String[]{

                "Acid Soil" ,
                "Acre" ,
                "Adsorb adsorption",
                "Agriculture:"   ,
                "Agriculture Extension Service",
                "Agronomy",
                "Alfalfa",
                "Alluvial Soil",
                "Animal Unit",
                "Annual",
                "Apiary" ,
                "Aquifer. " ,
                "Artificial Insemination" ,
                "Auger" ,
                "Avian:",
                "Balance Ration" ,
                "Banded." ,
                "Biennial rotation. " ,
                "Biological control.",
                "Biomass–energy ",
                "Biotic. " ,
                "Bloating: " ,
                "Boar: " ,
                "Break-even price." ,
                "Breed:  "
        };
        return source;
    }
    public static String[]getBiology(){
        String[] source = new String[]{

                "Acid Soil" ,
                "Acre" ,
                "Adsorb adsorption",
                "Agriculture:"   ,
                "Agriculture Extension Service",
                "Agronomy",
                "Alfalfa",
                "Alluvial Soil",
                "Animal Unit",
                "Annual",
                "Apiary" ,
                "Aquifer. " ,
                "Artificial Insemination" ,
                "Auger" ,
                "Avian:",
                "Balance Ration" ,
                "Banded." ,
                "Biennial rotation. " ,
                "Biological control.",
                "Biomass–energy ",
                "Biotic. " ,
                "Bloating: " ,
                "Boar: " ,
                "Break-even price." ,
                "Breed:  "
        };
        return source;
    }
    public static String[]getMusic(){
        String[] source = new String[]{

                "Acid Soil" ,
                "Acre" ,
                "Adsorb adsorption",
                "Agriculture:"   ,
                "Agriculture Extension Service",
                "Agronomy",
                "Alfalfa",
                "Alluvial Soil",
                "Animal Unit",
                "Annual",
                "Apiary" ,
                "Aquifer. " ,
                "Artificial Insemination" ,
                "Auger" ,
                "Avian:",
                "Balance Ration" ,
                "Banded." ,
                "Biennial rotation. " ,
                "Biological control.",
                "Biomass–energy ",
                "Biotic. " ,
                "Bloating: " ,
                "Boar: " ,
                "Break-even price." ,
                "Breed:  "
        };
        return source;
    }
    public static String[]getMathematics(){
        String[] source = new String[]{

                "Acid Soil" ,
                "Acre" ,
                "Adsorb adsorption",
                "Agriculture:"   ,
                "Agriculture Extension Service",
                "Agronomy",
                "Alfalfa",
                "Alluvial Soil",
                "Animal Unit",
                "Annual",
                "Apiary" ,
                "Aquifer. " ,
                "Artificial Insemination" ,
                "Auger" ,
                "Avian:",
                "Balance Ration" ,
                "Banded." ,
                "Biennial rotation. " ,
                "Biological control.",
                "Biomass–energy ",
                "Biotic. " ,
                "Bloating: " ,
                "Boar: " ,
                "Break-even price." ,
                "Breed:  "
        };
        return source;
    }
    public static String[]getGovernment(){
        String[] source = new String[]{

                "Acid Soil" ,
                "Acre" ,
                "Adsorb adsorption",
                "Agriculture:"   ,
                "Agriculture Extension Service",
                "Agronomy",
                "Alfalfa",
                "Alluvial Soil",
                "Animal Unit",
                "Annual",
                "Apiary" ,
                "Aquifer. " ,
                "Artificial Insemination" ,
                "Auger" ,
                "Avian:",
                "Balance Ration" ,
                "Banded." ,
                "Biennial rotation. " ,
                "Biological control.",
                "Biomass–energy ",
                "Biotic. " ,
                "Bloating: " ,
                "Boar: " ,
                "Break-even price." ,
                "Breed:  "
        };
        return source;
    }
    public static String[]getManagement(){
        String[] source = new String[]{

                "Acid Soil" ,
                "Acre" ,
                "Adsorb adsorption",
                "Agriculture:"   ,
                "Agriculture Extension Service",
                "Agronomy",
                "Alfalfa",
                "Alluvial Soil",
                "Animal Unit",
                "Annual",
                "Apiary" ,
                "Aquifer. " ,
                "Artificial Insemination" ,
                "Auger" ,
                "Avian:",
                "Balance Ration" ,
                "Banded." ,
                "Biennial rotation. " ,
                "Biological control.",
                "Biomass–energy ",
                "Biotic. " ,
                "Bloating: " ,
                "Boar: " ,
                "Break-even price." ,
                "Breed:  "
        };
        return source;
    }
}
