package com.gbarzy.godwin.teecha;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class BookmarkFragment extends Fragment {
    // private  String value = "Hello World!!";
    ArrayList<Word> allWordFromBookmark = new ArrayList<>();
    private FragmentListener listener;
    private DBHelper mDbHelper;
    private BookmarkAdapter adapter;
    private DBHelper mDBhelper;


    public BookmarkFragment() {
        // Required empty public constructor
    }

    public static BookmarkFragment getNewInstance(DBHelper dbHelper) {
        BookmarkFragment fragment = new BookmarkFragment();
        fragment.mDbHelper = dbHelper;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mDbHelper = new DBHelper(getActivity());
        this.allWordFromBookmark = mDbHelper.getAllWordFromBookmark();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bookmark, container, false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        ListView bookmarkList = (ListView) view.findViewById(R.id.bookmarkList);
        adapter = new BookmarkAdapter(getActivity(), this.allWordFromBookmark);
        bookmarkList.setAdapter(adapter);

        adapter.setOnItemClick(new ListItemListener() {
            @Override
            public void onItemClick(int position) {
                if (listener != null){
                    String key = ((Word) adapter.getItem(position)).key;
//                    Toast.makeText(getActivity(), "key = " +key, Toast.LENGTH_SHORT).show();

                    listener.onItemClick(key);

                }
            }
        });


        adapter.setOnItemDeleteClick(new ListItemListener() {
            @Override
            public void onItemClick(int position) {
                String key = ((Word) adapter.getItem(position)).key;
                adapter.removeItem(position);
                DBHelper.removeBookmark(key);
                BookmarkFragment.this.allWordFromBookmark.remove(position);
                adapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    public void setOnFragmentListener(FragmentListener listener) {
        this.listener = listener;
    }

//    String[] getListOfWords() {
//
//
//
//        ArrayList<String> _bookmarks = new DBHelper(getActivity()).getAllWordFromBookmark("key");
//
//        String[] bkmks = new String[_bookmarks.size()];
//
//        for (int i = 0; i < _bookmarks.size(); i++) {
//            bkmks[i] = _bookmarks.get(i);
//        }
//
//
//        return bkmks;
//    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_clear, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_clear) {
            mDbHelper.clearBookmark();
            adapter.clear();
            adapter.notifyDataSetChanged();
        }
        return super.onOptionsItemSelected(item);
    }
}
