package com.gbarzy.godwin.teecha;

/**
 * Created by TEXT-GODWIN on 10/9/2018.
 */
public class Word {
    public String key = " ";
    public String value = "";
    public String category="";

    public Word(){

    }

    public Word(String key, String value){
        this.key = key;
        this.value = value;
    }

    public Word(String key, String value, String category) {
        this.key = key;
        this.value = value;
        this.category = category;
    }
}
