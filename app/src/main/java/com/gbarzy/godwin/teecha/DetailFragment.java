package com.gbarzy.godwin.teecha;

import android.content.Context;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;


public class DetailFragment extends Fragment {
    public String definition;
    private TextView tvWord;
    private ImageView btnBookmark, btnVolume;
    private String value = "";
    private boolean isBookmarked = false;
    private WebView tvWordTranslate;
    private DBHelper mDBHelper;
    private TextToSpeech textToSpeech;

    public DetailFragment() {
        // Required empty public constructor
    }

    public static DetailFragment getNewInstance(String value) {
        return getNewInstance(value, false);
    }


    public static DetailFragment getNewInstance(String value, boolean fromBookmark) {
        DetailFragment fragment = new DetailFragment();
        fragment.value = value;
        fragment.isBookmarked = fromBookmark;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mDBHelper = new DBHelper(getActivity());

        //setup text to speach
        textToSpeech = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                textToSpeech.setLanguage(Locale.US);
                textToSpeech.setSpeechRate(0.9f);
            }
        });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);


    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvWord = (TextView) view.findViewById(R.id.tvWord);
        tvWordTranslate = (WebView) view.findViewById(R.id.tvWordTranslate);
        btnVolume = (ImageView) view.findViewById(R.id.btnVolume);
        btnBookmark = (ImageView) view.findViewById(R.id.btnBookmark);

        //setting title
        tvWord.setText(value.toUpperCase());
//        getting dictionary
        final String id = isBookmarked ? "0" : Global.getState(getActivity(), "dic_type");
        final Word word = this.mDBHelper.getWord(value.toUpperCase(), Integer.parseInt(id));

        this.definition = word.value;
        //setting it in web view
        tvWordTranslate.setWebViewClient(new WebViewClient());
        tvWordTranslate.loadData(word.value, "text/html", null);


        btnVolume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textToSpeech.isSpeaking()) {
                    int er_suc = textToSpeech.stop();

//                    Toast.makeText(getActivity(), "v = " + er_suc, Toast.LENGTH_SHORT).show();
                }
//                else{
                speak();

//                }
            }
        });

        //setting bookmark images
        if (this.mDBHelper.isWordMarked(word) || isBookmarked) {
            btnBookmark.setTag(1);
            btnBookmark.setImageResource(R.drawable.ic_bookmark_black);
        } else {
            btnBookmark.setImageResource(R.drawable.ic_bookmark);
            btnBookmark.setTag(0);
        }


        btnBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = (int) btnBookmark.getTag();
                if (i == 0) {
                    btnBookmark.setImageResource(R.drawable.ic_bookmark_black);
                    btnBookmark.setTag(1);
                    DetailFragment.this.mDBHelper.addBookmark(word, Integer.parseInt(id));
                } else if (i == 1) {
                    btnBookmark.setImageResource(R.drawable.ic_bookmark);
                    btnBookmark.setTag(0);
                    DBHelper.removeBookmark(value);
                }
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onResume() {
        super.onResume();

        //enabling search in detial fragment using autocomplete textbox
        AutoCompleteTextView autoCompleteTextView = (getActivity()) != null ? ((MainActivity) getActivity()).edit_search : null;
        if (autoCompleteTextView != null) {
            autoCompleteTextView.setThreshold(1);

            ArrayList<String> dic_type = new DBHelper(getActivity()).getWord(Integer.parseInt(Global.getState(getActivity(), "dic_type")));


            final DictionaryFragment dictionaryFragment = ((MainActivity) getActivity()).dictionaryFragment;

            final ArrayAdapter<String> adapter = dictionaryFragment.getAdapter();


            autoCompleteTextView.setAdapter(adapter);

            autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    Toast.makeText(getActivity(), "" + position, Toast.LENGTH_SHORT).show();
                    if (dictionaryFragment.listener != null) {
                        String selection = (String) parent.getItemAtPosition(position);

                        dictionaryFragment.listener.onItemClick(selection);
                    }
                }
            });

        }


    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    public void speak(/*String toSpeak*/) {
//        if (this.textToSpeech.isSpeaking())
//            this.textToSpeech.stop();

        textToSpeech.speak(this.definition, TextToSpeech.QUEUE_FLUSH, null);
    }

//    public void speak() {
//        speak("");
//    }

    @Override
    public void onPause() {
        super.onPause();
        if (textToSpeech.isSpeaking())
            textToSpeech.stop();
    }
}

