ABSORBENT GROUND :A chalk  ground  that  absorbs oil  and  is  used  in  oil  painting  to achieve  a  matt  effect  and  to  speed  up  drying. 
ACRYLIC  EMULSION :A water  dispersion  of polymers or polymers of  acrylic  acid, meth acrylic acid,  or  acrylonitrile.  Acrylic  emulsions dry by evaporation  of the  water  and  film  coalescence. 
ACRYLIC  SOLUTION: A solution  of  acrylic  resin  in  a  volatile  solvent.  Paints  made with  an  acrylic  solution  binder  resemble  oil  paints  more  than those  made  with  acrylic  emulsion  binders. 
ADDITIVE COLOR: colour that results from the mixture of two or more coloured lights, the visual blending of separate spots of transmitted coloured light.  
ALKYD : Synthetic resin used in paints and mediums. Paints are good example of alkyd paints. 
ALLA PRIMA:     Technique in which the final surface of a painting is completed in one sitting, without under painting.
ANHYDROUS :    Free from water.   
ARCHIVAL  :    Refers to materials that meet certain criteria for permanence such as lignin-free, pH neutral, alkaline-buffered, stable in light, etc.   
ASTM     : The American Society for Testing and Materials. An independent standard for certain paint qualities, adopted by most manufacturers.
BINDER  :    The non-volatile adhesive liquid portion of a paint that attaches pigment particles and the paint film as a whole to the support.  
BISTRE   :   A brown, transparent pigment.  
BLEEDING  :    In artwork, the effect of a dark colour seeping through a lighter colour to the surface.   
BLENDING   :   Smoothing the edges of two colours together so that they have a smooth gradation where they meet.   
BLOOM    :  A dull, progressively opaque, white effect caused on varnished surfaces by damp conditions.  
BODY COLOR:     Opaque paint, such as gouache, which has the covering power to obliterate underlying colour.   
BRUSHWORK  :   The characteristic way each artist brushes paint onto a support.    
CANVAS  :    Closely woven cloth used as a support for paintings.   
CARTOON  :  Other than what we watch on TV, it is a planning device in mural painting, often a full-scale line drawing of the design, without colour and tone.   
CASEIN    :  A natural protein obtained from cow's milk. Produces a flat, water-resistant film.  
CHIAROSCURO :    Term is used to describe the effect of light and shade in a painting or drawing, especially where strong tonal contrasts are used.  
CROSSHATCHING:    More than one set of close parallel lines that crisscross each other at angles, to model and indicate tone.   
CHROMA    :  The relative intensity or purity of a hue when compared to greyness or lack of hue.  
COCKLING   :   Wrinkling or puckering in paper supports, caused by applying washes onto a flimsy or improperly stretched surface.   
COLLAGE    :  A technique of picture making in which the artist uses materials other than the traditional paint, such as cut paper, wood, sand, and so on.  
COMPOSITION :    The arrangement of elements by an artist in a painting or drawing.  
CO-POLYMERS  :   A polymer in which the molecule is of more than one type of structural unit.  
COPAL    :  A hard resin used in making varnishes and painting mediums.     
DAMAR     :  A resin from conifer trees, used in making oil mediums and varnishes.  
DEAD COLOR :    A term for colours used in underpainting.  
DECKLE EDGE :    The ragged edge found on handmade papers.  
DECOUPAGE    : The act of cutting out paper designs and applying them to a surface to make an all over collage.   
DESIGNER COLORS :   Best quality Gouache paints, often used in commercial art.   
DILUENTS   :   Liquids, such as turpentine, used to dilute oil paint, the diluent for water based is water.   
DISPERSION  :   Applied to paint, a smooth, homogeneous mixture of ingredients; the process of dispersal, in which pigment particles are evenly distributed throughout the vehicle.
DISTEMPER   :   A blend of glue, chalk and water-based paint, used mostly for murals and posters.   
DRIER   (MATERIAL): A material that accelerates or initiates the drying of an oil paint or oil by promoting oxidation.    
DRYING OIL  :    An oil that, when spread into a thin layer and exposed to air, absorbs oxygen and converts into a tough film.    
EMULSION     :  A liquid in which small droplets of one liquid are immiscible in, but thoroughly and evenly dispersed throughout, a second liquid. i.e. Acrylic Emulsion   
ENCAUSTIC    :  Literally, to burn in. A painting technique in which the binder is melted wax.    
FATA: term used to describe paints which have a high oil content.   
FILLER:       Inert pigment added to paint to increase its bulk, also called extender.   
FILM   :     A thin coating or layer of paint, ink, etc.  
FIXATIVE :      A solution, usually of shellac and alcohol, sprayed onto drawings, to prevent their smudging or crumbling off the support. 
FRESCO   :    A painting technique in which the pigments are dispersed in plain water and applied to a damp plaster wall. The wall becomes the binder, as well as the support.  
FUGITIVE COLORS   :  Pigment or dye colours that fade when exposed to light.     
GESSO   :    A white ground material for preparing rigid supports forpainting, made of a mixture of chalk, white pigment, and glue. Same name applied to acrylic bound chalk and pigment used on flexible supports as well as rigid.   
GLAZE    :   A very thin, transparent coloured paint applied over a previously painted surface to alter the appearance and colour of the surface.  
GOUACHE   :    Opaque watercolours used for illustrations.   
GRISSAILLE :A monochromatic painting, usually in grey, which can be used under coloured glazes.  
GROUND: Coating material, usually white, applied to a support to make it ready for painting.   
GUM  :      A plant substance that is soluble in water.   
GUM ARABIC  :    A gum, extracted fro Acacia trees, used in solution as a medium for watercolour paints.  
HATCHING    :   A technique of modelling, indicating tone and suggesting light and shade in drawing or tempera panting, using closely set parallel line.   
HUE      :  The perceived colour of an object, identified by a common name such as red, orange, blue.   
HYGROSCOPIC  :    Absorbing or attracting moisture from the air.    
IMPASTO     :     A style of painting characterized by thick, juicy colour application.   
IMPRIMATURA  :    A thin, veil of paint, or paint-tinted size, applied to a ground to lessen the ground's absorbency or to tint the ground to a middle value.   
INTENSITY  : The purity and brightness of a colour. Also called saturation.    
KEY      :  Used to describe the prevailing tone of a painting. A predominantly light painting is said to have a high key. In contemporary mural painting, the key is the result of scratching a walls surface to prepare for final layer of plaster. similar to "tooth."   
LAKE      :  A dye that has been chemically or electrically attached to a particle and does not bleed or migrate. 
LATEX    :   A dispersion in water of a solid polymeric material.    
LEACHING  :     The process of drawing out excess liquid through a porous substance.    
LEAN :Used as an adjective to describe paint thinned with a spirit, which therefore has low oil content.    
LEVIGATING :     A method of water-washing pulverized pigments to clear the particles of dissolved salts or organic matter.    
LIGHTFAST  :    Resistant to fading or other changes due to light.    
LOCAL COLOR :     The actual colour of an object or surface, unaffected by shadow colouring, light quality or other factors   
LOOM STATE  :    Canvas that has not been primed, sized or otherwise prepared beforehand for painting.  
MAROUFLAGE  :    A technique for attaching, with glue, mural size painting on paper or fabric to a wall.   
MASSTONE    :  The top tone or body colour of a paint seen only by reflected light.   
MAT  :      A stiff cardboard with a window cut out of the centre, attached to a backboard.   
MATTE :      Flat, non-glossy; having a dull surface appearance. Variant spelling . matt. 
MEDIUM :      The liquid in which pigments are suspended. Also a material chosen by the artist for working. Plural is media.    
MIGRATION  :    The action of a pigment or dye moving through a dried film above or below it.  
MIXED MEDIA :     In drawing and painting this refers to the use of different media in the same picture.    
MONOMER  :     A material with low molecular weight that can react with similar or dissimilar materials to form a polymer.    
MOSAIC   :    Picture making technique using small units of variously coloured materials (glass, tile, stone) set in a mortar.    
MURAL :      Also referred to as wall painting. this word describes any painting made directly on the wall.   
MUSEUM BOARD   :  Multi ply board made of cotton rags or buffered cellulose to ensure chemical stability and neutrality.   
PALETTE: The surface which a painter will mix his colours. Also the range of colours used by an artist. 
PATINA  :     Originally the green brown encrustation on bronze, this now includes the natural effects of age or exposure on a surface.   
PENTIMENTO  :    A condition of old paintings where lead-containing pigments have become more transparent over time, revealing earlier layers.   
PIGMENTS  :     particles with inherent colour that can be mixed with adhesive binders to form paint. .   
PLASTICIZER  :    Ingredients added to paint to either make it flow or be easily re-dissolved.  
PLEIN AIR  :     French for "open air". Term describing paintings done outside directly from the subject.   
POLYMER  :     A series of monovers strung together in a repeating chainlike form. That really makes it clear.  
PRECIPITATE  :    An inert particle to which dyes can be laked.   
PRESERVATIVE  :    A material that prevents or inhibits the growth of micro-organisms in organic mixtures.   
PRIMER :      Coating material, usually white, applied to a support to prepare it for painting.   
PVA  :     Polyvinyl acetate, a manmade resin used as a paint medium and in varnish.  
REFRACTION :     The bending of light from one course in one medium to a different course through another medium of different refractive index. 
REFRACTIVE INDEX :    The numerical ratio of the speed of light in a vacuum to its speed in a substance. 
RESINS :      A general term for a wide variety of more or less transparent, fusible materials. The term is used to designate any polymer that is a basic material for paints and plastics.    
SANQUINE: A red-brown chalk.  
SAPONIFICATION :    The process in which a paint binder, under moist and alkaline conditions, becomes transparent or discoloured.    
SCUMBLING :     The technique of applying a thin, semi-opaque or translucent coating of paint over a previously painted surface to alter the colour or appearance of the surface without totally obscuring it.   
SECCO :      Italian for "dry". A technique of wall-painting onto dry plaster, or lime plaster that is dampened shortly before paint is applied.
SFUMATO :      Italian for "shaded off". Gradual, almost imperceptible transitions of colour from light to dark.   
SGRAFFITO :     Technique in which the surface layer is incised or cut away to reveal a contrasting colour.   
SHADE :      Term for a colour darkened with black.  
SHELLAC:       A yellow resin formed from secretions of the LAC insect, used in making varnish.   
SILICATE:       Material, such as sand, that is composed of a metal, oxygen, and silicon.   
SILVERPOINT:      A drawing method using a piece of metal, usually silver wire, drawn on a ground prepared with Chinese white, sometimes with pigment added.
SINOPIA: A red-brown chalk used for marking frescoes; also the preliminary drawing itself.   
SIZE: Material applied to a surface as a penetrating sealer, to alter or lessen its absorbency and isolate it from subsequent coatings.   
SKETCH:       A preliminary drawing of a composition.    
SQUARING UP:      A method for transferring an image to a larger or smaller format.    
STRAINER:       A wooden chassis for textile supports that has rigid, immovable corners.   
STRETCHER:      A wooden chassis for textile supports that has expandable corners.  
SUBTRACTIVE COLOR:     Colour resulting from the absorption of light.   
STUDY:       A detailed drawing or painting made of one or more parts of a final composition, but not the whole work.   
SUPPORT  BASE: The basic substrata of the painting; paper, cotton, linen, wall, etc.   
TEMPERA:       Technique of painting in which water and egg yolk or whole egg and oil mixture form the binder for the paint. Used also as a term for cheap opaque paints used in schools.  
THIXOTROPIC: Referring to materials that are thick and viscous while at rest but will flow if brushed, stirred, or shaken. Resumes its viscous state when the agitation stops.
TONER:       An unlaked dye that can bleed or migrate through dried paint films.  
TOOTH :      Small grained but even texture. Tooth provides for the attachment of succeeding layers of paint.   
TRACTION:       In oils, the movement of one paint layer over another.   
TRAGACANTH:      A gum, extracted from certain Astragalus plants, used as a binding agent in water colour paints and pastels.   
TROMPE L'OEIL :    French for "deceive the eye". A painting with extreme naturalistic details, aiming to persuade the viewer that they are looking at an actual object, not a representation.
UNDERPAINTING :     The traditional stage in oil painting of using a monochrome or dead colour as a base for composition. Also known as laying in.   
VALUE :      The relative lightness or darkness of a hue. Black is low value. White is a high value.    
VARNISH:       Generally, a more or less transparent film-forming liquid that dries into a solid film.    
VEDUTA :      Italian for "view". An accurate representation of an urban landscape.   
VEHICLE :      The entire liquid contents of a paint.   
VENICE TURPENTINE :   An oil resin - the semisolid mixture of a resin and an essential oil - derived from the larch and used primarily in making mediums and diluents for oil painting.   
VERDACCIO:     Old term for green underpainting.   
VOLATILE:      Evaporating rapidly or easily.   
VOLUME:      The space that an object or figure fills in a drawing or painting.    
WASH:      A thin, usually broadly applied, layer of transparent or heavily diluted paint or ink.    
WATERCOLOR:     A technique of painting using a binder made from a water-soluble gum. Watercolours can be transparent or opaque.
WATER TENSION BREAKER:   Substance added to water or to water-based paints in order to reduce surface tension. 
WAX RESIST:     The use of a waxy medium to make a design over which a coloured wash is spread.   
WET ON WET:     The application of fresh paint over an area on which the paint is still wet.   
WHITE SPIRITS:     A thinner used with oil paints replacing Turpentine.   
WHITING:      Chalk that is purified, ground with water and dried to form an inert pigment.
XYLOGRAPHY: Rarely  used  term  for  woodblock  printing.  Also  the  mechanical reproduction  of  wood  grain  for  decorative  purposes. 
YELLOWING: This  effect  on  oil  paintings  is  usually  caused  by  one  of  three reasons:  excessive  use  of  linseed  oil  medium;  applying  any of  the  varnishes  that  are  prone  to  yellow  with  age;  or  most often,  an  accumulation  of  dirt  embedded  into  the  varnish. 
ZOOMORPHIC: Describes  the  forms  of  works  of  art  and  ornaments  based  on animal  shapes.