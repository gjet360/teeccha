import os
import sqlite3
from datetime import datetime
print ('generating db')

db_file = datetime.now().isoformat().replace('-',' ').replace(':','.')

connect = sqlite3.connect(db_file)


def create_table(name):
    string = 'CREATE TABLE IF NOT EXISTS %s (key TEXT, value TEXT);'%(name)
    try:
        c = connect.cursor()
        c.execute(string)
    except Exception as e:
        print(e)

def insert_data(table_name, key, value):
    string = 'INSERT INTO %s (key, value) VALUES (?,?);' % (table_name)  
    try:
        c = connect.cursor()
        c.execute(string, (key, value))
        #print(c.lastrowid)
    except Exception as e:
        print(e)

        


#print (os.listdir('.'))

with connect:

    files = [f for f in os.listdir('.') if f.endswith('.txt')]

    print(files)

    input()

    count = 1
    for f in files:
        name_of_file = f.replace('.txt', '').lower()
        create_table(name_of_file)
        
        print('%s/%s' % (count, len(files)))

        count += 1

        with open(f, 'r') as infile:
            lines = infile.read().split('\n')
            #print (lines)

            c = 1
            for line in lines:
                print('\t%s/%s'%(c, len(lines) ))
                c+=1
                try:
                    data = line.split(':')
                    insert_data(name_of_file,data[0].strip(), data[1].strip())
                except Exception as e:
                    print(e)

